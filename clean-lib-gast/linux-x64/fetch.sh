#/bin/sh
GIT=git
GIT_BASEURL="https://gitlab.science.ru.nl/clean-and-itasks"

mkdir -p src
rm -rf src/gast
$GIT clone $GIT_BASEURL/gast src/gast
if [ -n "${CLEANDATE+set}" ]; then
	cd "src/gast"
	$GIT checkout `$GIT rev-list -n 1 --before="$CLEANDATE" master`
fi
