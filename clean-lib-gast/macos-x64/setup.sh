#!/bin/sh
# Get the latest clean-base system plus:
# - platform library 
# - generics library 
mkdir -p dependencies/clean
curl -L -o dependencies/clean-base.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-base-macos-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-platform.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-lib-platform-macos-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-generics.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-lib-generics-macos-x64-%Y%m%d.tgz`
(cd dependencies
 tar -xzf clean-base.tgz
 cp -r clean-base/* clean/
 tar -xzf clean-lib-platform.tgz
 cp -r clean-lib-platform/* clean/
 tar -xzf clean-lib-generics.tgz
 cp -r clean-lib-generics/* clean/
)
