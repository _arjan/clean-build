#!/bin/sh
mkdir -p target/clean-lib-gast

# Add libraries
mkdir -p target/clean-lib-gast/Libraries/Gast
cp -r src/gast/Libraries/* target/clean-lib-gast/Libraries/Gast/

# Add environments
mkdir -p target/clean-lib-gast/Config
cp src/gast/Config/windows-x86/Gast.env target/clean-lib-gast/Config/Gast.env
