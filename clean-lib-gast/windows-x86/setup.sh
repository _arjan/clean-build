#!/bin/sh
# Get the latest clean-base system plus:
# - platform library 
# - generics library 
mkdir -p dependencies/clean
curl -L -o dependencies/clean-base.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-base-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-platform.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-platform-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-generics.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-generics-windows-x86-%Y%m%d.zip`
(cd dependencies
 unzip clean-base.zip
 cp -r clean-base/* clean/
 unzip clean-lib-platform.zip
 cp -r clean-lib-platform/* clean/
 unzip clean-lib-generics.zip
 cp -r clean-lib-generics/* clean/
)
