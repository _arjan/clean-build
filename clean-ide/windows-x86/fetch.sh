#!/bin/sh
SVN=svn
SVN_BASEURL="https://svn.cs.ru.nl/repos"

mkdir -p src
$SVN export "$SVN_BASEURL/clean-ide/trunk" "src/CleanIDE"
