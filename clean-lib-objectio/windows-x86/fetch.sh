#!/bin/sh
SVN=svn
SVN_BASEURL="https://svn.cs.ru.nl/repos"

mkdir -p src

$SVN export "$SVN_BASEURL/clean-libraries/trunk/Libraries/ObjectIO" "src/ObjectIO"

$SVN export "$SVN_BASEURL/clean-libraries/trunk/Examples/ObjectIO" "src/ObjectIO-Examples"
