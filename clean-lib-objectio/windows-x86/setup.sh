#!/bin/sh
# Get the latest clean-base system plus:
# - argenv library 
# - directory library
# - stdlib library
mkdir -p dependencies/clean
curl -L -o dependencies/clean-base.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-base-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-argenv.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-argenv-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-directory.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-directory-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-stdlib.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-stdlib-windows-x86-%Y%m%d.zip`
(cd dependencies
 unzip clean-base.zip
 cp -r clean-base/* clean/
 unzip clean-lib-argenv.zip
 cp -r clean-lib-argenv/* clean/
 unzip clean-lib-directory.zip
 cp -r clean-lib-directory/* clean/
 unzip clean-lib-stdlib.zip
 cp -r clean-lib-stdlib/* clean/
)
