#!/bin/sh
mkdir -p target/clean-lib-platform

# Add libraries
mkdir -p target/clean-lib-platform/Libraries/Platform

cp -r src/clean-platform/src/libraries/OS-Independent/* target/clean-lib-platform/Libraries/Platform/
cp -r src/clean-platform/src/libraries/OS-Windows/* target/clean-lib-platform/Libraries/Platform/
cp -r src/clean-platform/src/libraries/OS-Windows-32/* target/clean-lib-platform/Libraries/Platform/
cp -r src/clean-platform/src/libraries/Platform-x86/* target/clean-lib-platform/Libraries/Platform/
