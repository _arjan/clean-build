#!/bin/sh
# Get the latest clean-base system
mkdir -p dependencies
curl -L -o dependencies/clean-base.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-base-windows-x86-%Y%m%d.zip`
(cd dependencies
 unzip clean-base.zip
)
