#!/bin/sh
mkdir -p target/clean-lib-platform

# Add libraries
mkdir -p target/clean-lib-platform/lib/Platform

cp -r src/clean-platform/src/libraries/OS-Independent/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/OS-Posix/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/OS-Linux/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/OS-Linux-64/* target/clean-lib-platform/lib/Platform/
cp -r src/clean-platform/src/libraries/Platform-x86/* target/clean-lib-platform/lib/Platform/
