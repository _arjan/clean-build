#cd "step64_2\Clean 2.2"
cd "step64_2\dist"

sed -e "s/\tHeapSize:\t\([0-9][0-9]*\)/\tHeapSize:\t4194304/" -e "s/StackSize:\t\([0-9][0-9]*\)/StackSize:\t819200/" < "Examples\ObjectIO Examples\scrabble\Scrabble.prj" > Scrabble.prj
mv Scrabble.prj "Examples\ObjectIO Examples\scrabble\Scrabble.prj"

sed -e "s/\tHeapSize:\t\([0-9][0-9]*\)/\tHeapSize:\t1048576/" -e "s/StackSize:\t\([0-9][0-9]*\)/StackSize:\t204800/" < "Examples\ObjectIO Examples\life\LifeGameExample.prj" > LifeGameExample.prj
mv LifeGameExample.prj "Examples\ObjectIO Examples\life\LifeGameExample.prj"

sed -e "s/\tHeapSize:\t\([0-9][0-9]*\)/\tHeapSize:\t4194304/" -e "s/StackSize:\t\([0-9][0-9]*\)/StackSize:\t2097152/" < "Examples\Small Examples\revtwice.prj" > revtwice.prj
mv revtwice.prj "Examples\Small Examples\revtwice.prj"

sed -e "s/\tHeapSize:\t\([0-9][0-9]*\)/\tHeapSize:\t2097152/" -e "s/StackSize:\t\([0-9][0-9]*\)/StackSize:\t2097152/" < "Examples\Small Examples\twice.prj" > twice.prj
mv twice.prj  "Examples\Small Examples\twice.prj"

sed -e "s/(size, pst)	= accPIO getProcessWindowSize pst/size = {w=1000,h=700}/" < "Examples\ObjectIO Examples\life\LifeGameExample.icl" > LifeGameExample.icl
mv LifeGameExample.icl "Examples\ObjectIO Examples\life\LifeGameExample.icl"

sed -r -e "s/Precompile:/\tPath:\t{Application}\\\Libraries\\\StdLib\n\tPrecompile:/" < "Examples\Generics\fromStr.prj" > fromStr.prj
mv fromStr.prj "Examples\Generics\fromStr.prj"

sed -r -e "s/Precompile:/\tPath:\t{Application}\\\Libraries\\\Generics\n\tPrecompile:/" < "Examples\Generics\toStr.prj" > toStr.prj
mv toStr.prj "Examples\Generics\toStr.prj"
