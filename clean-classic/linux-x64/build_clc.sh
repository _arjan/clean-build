set -e
# ./checkout.sh libraries/ArgEnvUnix
if test ! -d libraries/ArgEnvUnix ; then
  ./svn_checkout.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
fi
# ./checkout.sh compiler
./svn_checkout.sh clean-compiler/trunk compiler

cd compiler
clm -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert
unix/make.linux64.sh
cd ..

