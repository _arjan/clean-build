# ./checkout.sh libraries/ArgEnvUnix
if test ! -d libraries\ArgEnvUnix ; then
  ./svn_checkout.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
fi
# ./checkout.sh tools/htoclean
./svn_checkout.sh clean-tools/trunk/htoclean tools/htoclean

cd libraries/ArgEnvUnix
make -f Makefile ArgEnvC.o
cd ../..

cd tools/htoclean/htoclean\ source\ code
#clm -I unix -I ../../../libraries/ArgEnvUnix -l ../../../libraries/ArgEnvUnix/ArgEnvC.o -h 4m -nt -nr htoclean -o htoclean
clm -I unix -I ../../../libraries/ArgEnvUnix -h 4m -nt -nr htoclean -o htoclean
cd ../../..

