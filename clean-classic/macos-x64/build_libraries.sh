
# ./checkout.sh libraries/ArgEnvUnix
if test ! -d libraries/ArgEnvUnix ; then
  ./svn_checkout.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
fi

# ./checkout.sh libraries/GenLib
./svn_checkout.sh clean-libraries/trunk/Libraries/GenLib libraries/GenLib

# ./checkout.sh libraries/StdLib
./svn_checkout.sh clean-libraries/trunk/Libraries/StdLib libraries/StdLib

./svn_checkout.sh clean-libraries/trunk/Libraries/Directory libraries/Directory
cp tools/htoclean/Clean.h "libraries/Directory/Clean System Files Unix/Clean.h"
(cd "libraries/Directory/Clean System Files Unix"; gcc -c -O cDirectory.c)

./svn_checkout.sh clean-libraries/trunk/Libraries/MersenneTwister libraries/MersenneTwister

./svn_checkout.sh clean-libraries/trunk/Libraries/StdLib libraries/StdLib

./svn_checkout.sh clean-libraries/trunk/Libraries/TCPIP libraries/TCPIP
cp tools/htoclean/Clean.h "libraries/TCPIP/Linux_C/Clean.h"
(cd "libraries/TCPIP/Linux_C"; gcc -c -O cTCP_121.c)
(cd "libraries/TCPIP"; sed 's/, library "wsock_library"//' <ostcp.icl >ostcp.icl_; rm -f ostcp.icl; mv ostcp.icl_ ostcp.icl)
(cd "libraries/TCPIP/Linux_C"; sed 's/, library "wsock_library"//' <ostcp.icl >ostcp.icl_; rm -f ostcp.icl; mv ostcp.icl_ ostcp.icl)

./svn_checkout.sh clean-dynamic-system/trunk/dynamics/StdDynamicEnv libraries/StdDynamicEnv

cp -R Gast libraries
