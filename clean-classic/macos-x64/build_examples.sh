# ./checkout.sh CleanExamples/Small\ Examples
./svn_checkout.sh clean-libraries/trunk/Examples/Small\ Examples CleanExamples/Small\ Examples

mkdir -p clean/examples
for a in acker copyfile e fsieve hamming invperm lqueen mulmat nfib pascal reverse revtwice \
	rfib sieve squeen str_arit stwice tak twice war_seq;
do cp CleanExamples/Small\ Examples/$a.icl clean/examples;
done
cp txt/run_all_programs clean/examples
cp txt/Makefile_examples clean/examples/Makefile
