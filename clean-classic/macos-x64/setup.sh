#!/bin/sh
curl -L -o clean.zip ftp://ftp.cs.ru.nl/pub/Clean/nightly/clean-itasks-osx-20160630.zip
unzip clean.zip
mv "clean" "boot_compiler"
rm clean.zip
rmdir clean
cd boot_compiler
make
