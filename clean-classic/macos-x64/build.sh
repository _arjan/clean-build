set -e
export OLDPATH=$PATH
./build_cg.sh
./build_clm.sh
./build_rts.sh
export PATH=`pwd`/boot_compiler/bin:$OLDPATH
./build_htoclean.sh
./build_clc.sh
./build_libraries.sh
./build_clean.sh
./build_stdenv.sh
./build_examples.sh
export PATH=$OLDPATH
mv clean clean0
cd clean0
make
cd ..
export PATH=`pwd`/clean0/bin:$OLDPATH
mkdir step0
mkdir step0/libraries
mv libraries/StdEnv step0/libraries/StdEnv
mv libraries/ArgEnvUnix step0/libraries/ArgEnvUnix
mv libraries/GenLib step0/libraries/GenLib
mv libraries/StdLib step0/libraries/StdLib
mv compiler step0/compiler
mkdir step0/tools
mv tools/htoclean step0/tools/htoclean
mkdir step0/tools/CleanIDE
mv tools/CleanIDE/CleanLicenseConditions.txt step0/tools/CleanIDE/CleanLicenseConditions.txt
mkdir step0/tools/CleanIDE/Help
#mv tools/CleanIDE/Help/CleanLangRep.2.1.pdf step0/tools/CleanIDE/Help/CleanLangRep.2.1.pdf
mkdir step0/CleanExamples
mv "CleanExamples/Small Examples" "step0/CleanExamples/Small Examples"
./build_htoclean.sh
./build_clc.sh
./build_libraries.sh
./build_clean.sh
./build_stdenv.sh
./build_examples.sh
