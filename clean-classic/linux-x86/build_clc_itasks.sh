set -e
# ./checkout.sh libraries/ArgEnvUnix
if test ! -d libraries/ArgEnvUnix ; then
  ./svn_checkout.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
fi
# ./checkout.sh compiler
./svn_checkout.sh clean-compiler/branches/itask compiler

cd compiler
clm -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert
cd unix
sed "s/-h 80M -s 4m/-h 512m -s 16m/" <make.linux64.sh >make.linux64.sh_
rm -f make.linux64.sh
mv make.linux64.sh_ make.linux64.sh
chmod +x make.linux64.sh
cd ..
unix/make.linux64.sh
cd ..

