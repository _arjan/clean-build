if test ! -d libraries\ArgEnvUnix ; then
  ./svn_checkout.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
fi
./svn_checkout.sh clean-tools/trunk/elf_linker tools/elf_linker

cd libraries/ArgEnvUnix
make -f Makefile ArgEnvC.o
cd ../..

cd tools/elf_linker
clm -I . -I ai64 -I ../../libraries/ArgEnvUnix -I ../../compiler/main/Unix \
	-l ../../libraries/ArgEnvUnix/ArgEnvC.o \
	-l ../../compiler/main/Unix/set_return_code_c.o \
	-s 8m -h 64m -nt -nr linker -o linker
cd ../../..

