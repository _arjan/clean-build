# ./checkout.sh libraries/StdEnv
./svn_checkout.sh clean-libraries/trunk/Libraries/StdEnv libraries/StdEnv

mkdir -p clean/StdEnv
cp libraries/StdEnv/_library.dcl clean/StdEnv
cp libraries/StdEnv/_startup.dcl clean/StdEnv
cp libraries/StdEnv/_system.dcl clean/StdEnv
for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
	 StdFunc StdList StdMisc StdOrdList StdOverloaded StdOverloadedList \
	 StdStrictLists StdTuple _SystemArray _SystemEnum _SystemEnumStrict \
	 _SystemStrictLists StdGeneric;
do cp libraries/StdEnv/$a.[di]cl clean/StdEnv ;
done

cp libraries/StdEnv/StdInt.icl clean/StdEnv
cp libraries/StdEnv/StdInt.dcl clean/StdEnv
cp libraries/StdEnv/StdFile.icl clean/StdEnv
cp libraries/StdEnv/StdFile.dcl clean/StdEnv
cp libraries/StdEnv/StdReal.icl clean/StdEnv
cp libraries/StdEnv/StdReal.dcl clean/StdEnv
cp libraries/StdEnv/StdString.icl clean/StdEnv
cp libraries/StdEnv/StdString.dcl clean/StdEnv

cp txt/_startupProfile.dcl clean/StdEnv
cp txt/_startupTrace.dcl clean/StdEnv
cp txt/Makefile_stdenv clean/StdEnv/Makefile
cp txt/make.sh clean/StdEnv/make.sh
cp txt/install.sh clean/StdEnv/install.sh
cp txt/install_stdenv.sh clean/StdEnv/install_stdenv.sh

mkdir -p clean/StdEnv/Clean\ System\ Files
cp libraries/StdEnv/Clean\ System\ Files/_system.abc clean/StdEnv/Clean\ System\ Files
cp RuntimeSystem/_startup.o clean/StdEnv/Clean\ System\ Files
cp RuntimeSystem/_startupTrace.o clean/StdEnv/Clean\ System\ Files
cp RuntimeSystem/_startupProfile.o clean/StdEnv/Clean\ System\ Files

clean/exe/cg clean/StdEnv/Clean\ System\ Files/_system

echo first compile of system modules
for a in StdChar; # compile twice for inlining
do clean/exe/cocl -P clean/StdEnv $a ;
done

echo second compile of system modules
for a in StdMisc StdBool StdInt StdChar StdFile StdReal StdString;
do clean/exe/cocl -P clean/StdEnv $a ;
done

