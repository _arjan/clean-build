set -e
if test ! -d sapldynamics ; then
  ./svn_checkout.sh clean-dynamic-system/branches/itask sapldynamics
fi

cd sapldynamics
batch_build SaplCollectorLinkerLinux32.prj
cd ..
