#!/bin/sh

TARGET_DIR=target/clean-lib-sapl

# Copy Clean libraries
mkdir -p $TARGET_DIR/lib/Sapl
cp -r src/clean-sapl-hierarchical/src/* $TARGET_DIR/lib/Sapl

# Copy Sapl StdEnv
mkdir -p $TARGET_DIR/lib/StdEnv/Sapl
cp -r src/clean-sapl/platforms/Clean/stdlib/* $TARGET_DIR/lib/StdEnv/Sapl

# Build and copy collector linker
export CLEAN_HOME=`pwd`/dependencies/clean
export PATH=$CLEAN_HOME/bin:$PATH

mkdir -p build/sapl-collector-linker
cp -r src/sapl-dynamics/* build/sapl-collector-linker/

(cd build/sapl-collector-linker 
    cpm SaplCollectorLinkerMacOSX.prj
)
mkdir -p $TARGET_DIR/lib/exe
cp build/sapl-collector-linker/sapl-collector-linker $TARGET_DIR/lib/exe/

