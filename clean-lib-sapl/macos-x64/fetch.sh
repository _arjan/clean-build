#/bin/sh
SVN=svn
SVN_BASEURL="https://svn.cs.ru.nl/repos"
GIT=git
GIT_BASEURL="https://gitlab.science.ru.nl/clean-and-itasks"

mkdir -p src
$GIT clone --depth 1 $GIT_BASEURL/clean-sapl src/clean-sapl
$GIT clone --depth 1 -b hierarchical $GIT_BASEURL/clean-sapl src/clean-sapl-hierarchical

#The Sapl collector linker is in the dynamics repository for obscure dependency reasons
$SVN export $SVN_BASEURL/clean-dynamic-system/branches/itask src/sapl-dynamics
