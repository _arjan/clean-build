#!/bin/sh
# Get the latest clean-base system plus:
# - argenv library 
# - directory library
# - stdlib library
mkdir -p dependencies/clean
curl -L -o dependencies/clean-base.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-base-macos-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-argenv.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-lib-argenv-macos-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-directory.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-lib-directory-macos-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-stdlib.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-lib-stdlib-macos-x64-%Y%m%d.tgz`
(cd dependencies
 tar -xzf clean-base.tgz
 cp -r clean-base/* clean/
 tar -xzf clean-lib-argenv.tgz
 cp -r clean-lib-argenv/* clean/
 tar -xzf clean-lib-directory.tgz
 cp -r clean-lib-directory/* clean/
 tar -xzf clean-lib-stdlib.tgz
 cp -r clean-lib-stdlib/* clean/
)
# Add an extra environment
ENVIRONMENTS=dependencies/clean/etc/IDEEnvs
echo "\tEnvironment" >> $ENVIRONMENTS
echo "\t\tEnvironmentName:\tStdEnv SAPL" >> $ENVIRONMENTS
echo "\t\tEnvironmentPaths" >> $ENVIRONMENTS
echo "\t\t\tPath:\t{Application}/lib/StdEnv" >> $ENVIRONMENTS
echo "\t\tEnvironmentCompiler:\tlib/exe/cocl:-sapl" >> $ENVIRONMENTS
echo "\t\tEnvironmentCodeGen:\tlib/exe/cg" >> $ENVIRONMENTS
echo "\t\tEnvironmentLinker:\t/usr/bin/gcc" >> $ENVIRONMENTS
echo "\t\tEnvironmentDynLink:\t/usr/bin/gcc" >> $ENVIRONMENTS
echo "\t\tEnvironmentVersion:\t920" >> $ENVIRONMENTS
echo "\t\tEnvironmentRedirect:\tFalse" >> $ENVIRONMENTS
echo "\t\tEnvironmentCompileMethod:\tPers" >> $ENVIRONMENTS
echo "\t\tEnvironmentProcessor:\tI386"  >> $ENVIRONMENTS
echo "\t\tEnvironment64BitProcessor:\tTrue" >> $ENVIRONMENTS
