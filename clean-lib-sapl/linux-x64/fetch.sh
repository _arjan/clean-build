#/bin/sh
SVN=svn
SVN_BASEURL="https://svn.cs.ru.nl/repos"
GIT=git
GIT_BASEURL="https://gitlab.science.ru.nl/clean-and-itasks"

mkdir -p src
rm -rf src/clean-sapl src/clean-sapl-hierarchical
$GIT clone $GIT_BASEURL/clean-sapl src/clean-sapl
$GIT clone -b hierarchical $GIT_BASEURL/clean-sapl src/clean-sapl-hierarchical
if [ -n "${CLEANDATE+set}" ]; then
	cd "src/clean-sapl"
	$GIT checkout `$GIT rev-list -n 1 --before="$CLEANDATE" master`
	cd "../clean-sapl-hierarchical"
	$GIT checkout `$GIT rev-list -n 1 --before="$CLEANDATE" hierarchical`
	cd ../..
fi

REV=""
if [ -n "${CLEANDATE+set}" ]; then
	REV="-r {$CLEANDATE}"
fi

#The Sapl collector linker is in the dynamics repository for obscure dependency reasons
$SVN export $REV $SVN_BASEURL/clean-dynamic-system/branches/itask src/sapl-dynamics
