#!/bin/sh

TARGET_DIR=target/clean-lib-sapl

# Copy Clean libraries
mkdir -p $TARGET_DIR/Libraries/Sapl
cp -r src/clean-sapl-hierarchical/src/* $TARGET_DIR/Libraries/Sapl

# Copy Sapl StdEnv
mkdir -p $TARGET_DIR/Libraries/StdEnv/Sapl
cp -r src/clean-sapl/platforms/Clean/stdlib/* $TARGET_DIR/Libraries/StdEnv/Sapl

# Build and copy collector linker
export CLEAN_HOME=`cygpath --windows --absolute dependencies/clean`
export PATH=$CLEAN_HOME\\Tools:$PATH

mkdir -p build/sapl-collector-linker
cp -r src/sapl-dynamics/* build/sapl-collector-linker/

(cd build/sapl-collector-linker 
    sed "s/Target:\\tiTasks/Target:\\tStdEnv/" < SaplCollectorLinker.prj > SaplCollectorLinker_.prj
    ../../dependencies/clean/cpm.exe SaplCollectorLinker_.prj
)
mkdir -p $TARGET_DIR/Tools/Clean\ System
cp build/sapl-collector-linker/SaplCollectorLinker.exe $TARGET_DIR/Tools/Clean\ System/

