#!/bin/sh
set -e
mkdir -p target/clean-lib-itasks

# Add libraries
mkdir -p target/clean-lib-itasks/lib/iTasks
cp -r src/itasks-sdk/Libraries/iTasks* target/clean-lib-itasks/lib/iTasks/

# Add examples
mkdir -p target/clean-lib-itasks/examples/iTasks
cp -r src/itasks-sdk/Examples/* target/clean-lib-itasks/examples/iTasks/
mv target/clean-lib-itasks/examples/iTasks/BasicAPIExamples.prj.default target/clean-lib-itasks/examples/iTasks/BasicAPIExamples.prj

# Build tools
mkdir -p build/clean
cp -r dependencies/clean/* build/clean/

export CLEAN_HOME=`pwd`/build/clean
export PATH=$CLEAN_HOME/bin:$PATH

# Web collector
mkdir -p build/itasks-web-collector
mkdir -p target/clean-lib-itasks/lib/exe
cp -r src/itasks-sdk/Tools/WebResourceCollector.icl build/itasks-web-collector/
(cd build/itasks-web-collector
    clm -nr -nt -IL Generics -IL Platform WebResourceCollector -o itasks-web-collector 
)
cp build/itasks-web-collector/itasks-web-collector target/clean-lib-itasks/lib/exe/itasks-web-collector

# Add environments
mkdir -p target/clean-lib-itasks/etc
cp src/itasks-sdk/Config/macos-x64/iTasks.env target/clean-lib-itasks/etc/iTasks.env
