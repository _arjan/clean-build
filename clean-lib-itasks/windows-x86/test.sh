set -e
# Set up
mkdir -p "test/clean"
mkdir -p "test/tests"
cp -r dependencies/clean/* test/clean/
cp -r target/clean-lib-itasks/* test/clean/
cp -r src/itasks-sdk/Tests/* test/tests
tail -n +2 test/clean/Config/iTasks.env >> test/clean/Config/IDEEnvs

# Compile and run
export CLEAN_HOME=`cygpath --windows --absolute test/clean`
export PATH=$CLEAN_HOME\\Tools:$PATH
(cd "test"
 (cd "tests"
   # Compile unit tests
   #sed s/ShowConstructors/NoConsole/ < RunTests.prj.default > RunTests.prj
   #../clean/cpm.exe RunTests.prj
   # Execute unit tests
   #./RunTests.exe.exe --unit
 )
 (cd "clean/Examples/iTasks"
   # Compile examples (to check all examples still build)
   ../../cpm.exe BasicAPIExamples.prj
 )
)
