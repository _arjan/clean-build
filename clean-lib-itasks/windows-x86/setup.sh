#!/bin/sh
# Get the latest clean-base system plus:
# - platform library 
# - sapl library 
# - graph copy library
# - tcpip
mkdir -p dependencies/clean
curl -L -o dependencies/clean-base.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-base-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-platform.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-platform-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-sapl.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-sapl-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-graphcopy.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-graphcopy-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-tcpip.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-tcpip-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-generics.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-generics-windows-x86-%Y%m%d.zip`
curl -L -o dependencies/clean-lib-dynamics.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-dynamics-windows-x86-%Y%m%d.zip`
(cd dependencies
 unzip clean-base.zip
 cp -r clean-base/* clean/
 unzip clean-lib-platform.zip
 cp -r clean-lib-platform/* clean/
 unzip clean-lib-sapl.zip
 cp -r clean-lib-sapl/* clean/
 unzip clean-lib-graphcopy.zip
 cp -r clean-lib-graphcopy/* clean/
 unzip clean-lib-tcpip.zip
 cp -r clean-lib-tcpip/* clean/
 unzip clean-lib-generics.zip
 cp -r clean-lib-generics/* clean/
 unzip clean-lib-dynamics.zip
 cp -r clean-lib-dynamics/* clean/
)
