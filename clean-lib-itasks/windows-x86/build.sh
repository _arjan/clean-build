#!/bin/sh
mkdir -p target/clean-lib-itasks

# Add libraries
mkdir -p target/clean-lib-itasks/Libraries/iTasks
cp -r src/itasks-sdk/Libraries/iTasks* target/clean-lib-itasks/Libraries/iTasks/

# Add examples
mkdir -p target/clean-lib-itasks/Examples/iTasks
cp -r src/itasks-sdk/Examples/* target/clean-lib-itasks/Examples/iTasks/
mv target/clean-lib-itasks/Examples/iTasks/BasicAPIExamples.prj.default target/clean-lib-itasks/Examples/iTasks/BasicAPIExamples.prj

# Build tools
mkdir -p build/clean
cp -r dependencies/clean/* build/clean/


export CLEAN_HOME=`cygpath --windows --absolute build/clean`
export PATH=$CLEAN_HOME\\Tools:$PATH

# Web collector
mkdir -p build/itasks-web-collector
mkdir -p target/clean-lib-itasks/Tools/Clean\ System
cp -r src/itasks-sdk/Tools/WebResourceCollector.icl build/itasks-web-collector/
cp txt/WebResourceCollector.prj build/itasks-web-collector/
(cd build/itasks-web-collector
    ../../dependencies/clean/cpm.exe WebResourceCollector.prj
)
cp build/itasks-web-collector/WebResourceCollector.exe target/clean-lib-itasks/Tools/Clean\ System/WebResourceCollector.exe

# Add environments
mkdir -p target/clean-lib-itasks/Config
cp src/itasks-sdk/Config/windows-x86/iTasks.env target/clean-lib-itasks/Config/iTasks.env
