#!/bin/sh
set -e
# Set up
mkdir -p "test/clean"
mkdir -p "test/tests"
cp -r dependencies/clean/* test/clean/
cp -r target/clean-lib-itasks/* test/clean/
cp -r src/itasks-sdk/Tests/TestPrograms test/tests
cp src/itasks-sdk/Tools/RunUnitTestsForCI.icl test
tail -n +2 test/clean/etc/iTasks.env >> test/clean/etc/IDEEnvs

# Compile and run
(cd "test"
 export CLEAN_HOME=`pwd`/clean
 export PATH=$CLEAN_HOME/bin:$PATH

 # Compile unit test runner
 cpm project RunUnitTestsForCI create 
 # Set environment..
 sed "s/Target.*$/Target: iTasks/" < RunUnitTestsForCI.prj > RunUnitTestsForCI_.prj
 cpm RunUnitTestsForCI_.prj

 # Execute unit tests
 ./RunUnitTestsForCI.exe ./tests/TestPrograms
)
