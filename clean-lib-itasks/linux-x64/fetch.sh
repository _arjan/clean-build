#/bin/sh
set -e
GIT=git
GIT_BASEURL="https://gitlab.science.ru.nl/clean-and-itasks"

mkdir -p src
rm -rf src/itasks-sdk
$GIT clone $GIT_BASEURL/itasks-sdk src/itasks-sdk
if [ -n "${CLEANDATE+set}" ]; then
	cd "src/itasks-sdk"
	$GIT checkout `$GIT rev-list -n 1 --before="$CLEANDATE" master`
fi
