#!/bin/sh
# Get the latest clean-base system plus:
# - platform library 
# - sapl library 
# - graph copy library
# - tcpip
mkdir -p dependencies/clean
curl -L -o dependencies/clean-base.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-base-linux-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-platform.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-lib-platform-linux-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-sapl.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-lib-sapl-linux-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-graphcopy.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-lib-graphcopy-linux-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-tcpip.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-lib-tcpip-linux-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-generics.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-lib-generics-linux-x64-%Y%m%d.tgz`
curl -L -o dependencies/clean-lib-dynamics.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-lib-dynamics-linux-x64-%Y%m%d.tgz`
(cd dependencies
 tar -xzf clean-base.tgz
 cp -r clean-base/* clean/
 tar -xzf clean-lib-platform.tgz
 cp -r clean-lib-platform/* clean/
 tar -xzf clean-lib-sapl.tgz
 cp -r clean-lib-sapl/* clean/
 tar -xzf clean-lib-graphcopy.tgz
 cp -r clean-lib-graphcopy/* clean/
 tar -xzf clean-lib-tcpip.tgz
 cp -r clean-lib-tcpip/* clean/
 tar -xzf clean-lib-generics.tgz
 cp -r clean-lib-generics/* clean/
 tar -xzf clean-lib-dynamics.tgz
 cp -r clean-lib-dynamics/* clean/
)
