#!/bin/sh
set -e
cd target
tar -czf `date +clean-lib-itasks-linux-x64-%Y%m%d.tgz` clean-lib-itasks
