#!/bin/sh
# Get all the packages:
NAME="clean-bundle-complete"
PACKAGES="base ide lib-argenv lib-directory lib-dynamics lib-generics lib-graphcopy lib-gast lib-itasks lib-platform lib-objectio lib-sapl lib-stdlib lib-tcpip"
mkdir -p src

# Download and unpack additional libraries
for PKG in $PACKAGES; do
	curl -L -o src/clean-$PKG.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-$PKG-windows-x86-%Y%m%d.zip`
	(cd src; unzip clean-$PKG.zip)
done
