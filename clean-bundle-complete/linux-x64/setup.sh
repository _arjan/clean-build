#!/bin/sh
PACKAGES="lib-argenv lib-directory lib-dynamics lib-generics lib-graphcopy lib-gast lib-itasks lib-platform lib-sapl lib-stdlib lib-tcpip"

for PKG in $PACKAGES; do
	../../clean-$PKG/linux-x64/setup.sh
done
