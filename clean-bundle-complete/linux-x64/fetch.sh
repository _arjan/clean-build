#!/bin/sh
# Get all the packages:
NAME="clean-bundle-complete"
PACKAGES="lib-argenv lib-directory lib-dynamics lib-generics lib-graphcopy lib-gast lib-itasks lib-platform lib-sapl lib-stdlib lib-tcpip"
mkdir -p src

curl -L -o src/clean-base.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-base-linux-x64-%Y%m%d.tgz`
(cd src; tar -xzf clean-base.tgz)

# Download and unpack additional libraries
for PKG in $PACKAGES; do
	echo $PKG
	../../clean-$PKG/linux-x64/fetch.sh
done
