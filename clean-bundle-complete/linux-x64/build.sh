#!/bin/sh
NAME="clean-bundle-complete"
PACKAGES="lib-argenv lib-directory lib-dynamics lib-generics lib-graphcopy lib-gast lib-itasks lib-platform lib-sapl lib-stdlib lib-tcpip"

mkdir -p target/$NAME
cp -r src/clean-base/* target/$NAME/
# Add additional libraries
for PKG in $PACKAGES; do
	../../clean-$PKG/linux-x64/build.sh
	rsync -r target/clean-$PKG/* target/$NAME
	rm -rf target/clean-$PKG
done
# Install environments
for ENV in target/$NAME/etc/*.env; do
	tail -n +2 $ENV >> target/$NAME/etc/IDEEnvs	
done
