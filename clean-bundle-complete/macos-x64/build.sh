#!/bin/sh
NAME="clean-bundle-complete"
PACKAGES="base lib-argenv lib-directory lib-dynamics lib-generics lib-graphcopy lib-gast lib-itasks lib-platform lib-sapl lib-stdlib lib-tcpip"

mkdir -p target/$NAME
# Add additional libraries
for PKG in $PACKAGES; do
	cp -r src/clean-$PKG/* target/$NAME/
done
# Install environments
for ENV in target/$NAME/etc/*.env; do
	tail -n +2 $ENV >> target/$NAME/etc/IDEEnvs	
done
