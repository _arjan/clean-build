#!/bin/sh
# Get all the packages:
NAME="clean-bundle-complete"
PACKAGES="base lib-argenv lib-directory lib-dynamics lib-generics lib-graphcopy lib-gast lib-itasks lib-platform lib-sapl lib-stdlib lib-tcpip"
mkdir -p src

# Download and unpack additional libraries
for PKG in $PACKAGES; do
	curl -L -o src/clean-$PKG.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-$PKG-macos-x64-%Y%m%d.tgz`
	(cd src; tar -xzf clean-$PKG.tgz)
done
