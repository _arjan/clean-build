#!/bin/sh
# Get the latest clean-base system
mkdir -p dependencies/clean
curl -L -o dependencies/clean-base.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-base-linux-x64-%Y%m%d.tgz`
(cd dependencies
 tar -xzf clean-base.tgz
)
