#!/bin/sh
TARGET=target/clean-lib-argenv

export CLEAN_HOME=`pwd`/dependencies/clean-base
export PATH=$CLEAN_HOME/bin:$PATH

mkdir -p build
cp -r src/ArgEnvUnix build/ArgEnv
(cd build/ArgEnv
	make -e
)

mkdir -p "$TARGET/lib/ArgEnv/Clean System Files"
for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README;
do  cp build/ArgEnv/$f $TARGET/lib/ArgEnv/$f
done
cp "build/ArgEnv/Clean System Files/ArgEnvC.o" "$TARGET/lib/ArgEnv/Clean System Files/"
