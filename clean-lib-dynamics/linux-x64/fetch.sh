#!/bin/sh
SVN=svn
SVN_BASEURL="https://svn.cs.ru.nl/repos"

REV=""
if [ -n "${CLEANDATE+set}" ]; then
	REV="-r {$CLEANDATE}"
fi

mkdir -p src
rm -rf src/StdDynamicEnv
$SVN export $REV "$SVN_BASEURL/clean-dynamic-system/trunk/dynamics/StdDynamicEnv" "src/StdDynamicEnv"
