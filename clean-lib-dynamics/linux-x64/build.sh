#!/bin/sh
mkdir -p target/clean-lib-dynamics/lib/Dynamics

cp src/StdDynamicEnv/extension/StdCleanTypes.dcl target/clean-lib-dynamics/lib/Dynamics
cp src/StdDynamicEnv/extension/StdCleanTypes.icl target/clean-lib-dynamics/lib/Dynamics
cp src/StdDynamicEnv/extension/StdDynamic.dcl target/clean-lib-dynamics/lib/Dynamics
cp src/StdDynamicEnv/extension/StdDynamicNoLinker.icl target/clean-lib-dynamics/lib/Dynamics/StdDynamic.icl
cp src/StdDynamicEnv/implementation/_SystemDynamic.dcl target/clean-lib-dynamics/lib/Dynamics
cp src/StdDynamicEnv/implementation/_SystemDynamic.icl target/clean-lib-dynamics/lib/Dynamics
