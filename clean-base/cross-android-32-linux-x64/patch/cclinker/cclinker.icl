/*
 * Translate cpm linker-options-file to linker specified in cclinker.config file.
 *
 * Example cclinker.config file:
 * cc      /usr/bin/gcc
 * cflags  -g 
 */
module cclinker

import StdEnv
import System.CommandLine
import System.File, System.FilePath, System.Directory
import System.Process
import Data.Error
import Text

Start :: *World -> *World
Start world
	//Figure out input and output dirs
	# (argv,world) = getCommandLine world
	| length argv <> 5 = print "usage: cclinker -I <linker-options-file> -O <linker-error-path>" world // fail.
	# configFile = (takeDirectory (argv !! 0)) </> "cclinker.config"
	# (config,world) = readFile configFile world
	| isError config
		= print ("config file: " +++ configFile +++ " " +++ (toString (fromError config))) world
	# config = fromOk config
	# cc = lookupKey config "cc"
	# cflags = trim (lookupKey config "cflags")
	# world = print ("cc: " +++ cc +++ ", cflags: " +++ cflags) world
	# (content,world) = readFile (argv !! 2) world //When called by the IDE or cpm the third argument will be the 'linkopts' file
	| isError content 
		= print (toString (fromError content)) world 
	# content = fromOk content
	# outFile = (lookupExePath content)
	# outDir = takeDirectory outFile
	# objectFiles = (lookupObjectPaths content)
	# args = ["-o", outFile, "-lc", "-lm"] ++ (if (cflags == "") [] (split " " cflags))
	# (exitCode, world) = callProcess cc (objectFiles ++ args) (Just outDir) world	
	= world

//Print a debug message
print msg world
	# (console,world) = stdio world
	# console = fwrites "cclinker : " console
	# console = fwrites msg console
	# console = fwrites "\n" console
	# (_,world) = fclose console world
	= world

//Brute force 'parsing' functions that find the necessary information in a linkopts file
lookupExePath :: String -> String
lookupExePath linkopt
	= lookupKey linkopt "ExePath"

lookupKey :: String String -> String
lookupKey content key
	= last (split "\t" (hd (filter (startsWith key) (split "\n" content))))

lookupObjectPaths :: String -> [String]
lookupObjectPaths linkopts
	= map (last o split "\t") (takeWhile (startsWith "\tPath") (tl (dropWhile (not o (startsWith "ObjectPaths")) (split "\n" linkopts))))
