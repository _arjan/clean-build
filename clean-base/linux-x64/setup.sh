#!/bin/sh
mkdir -p dependencies
curl -L -o dependencies/clean.tar.gz http://clean.cs.ru.nl/download/Clean24/linux/clean2.4_64.tar.gz
(cd dependencies
	tar -xzf clean.tar.gz
	(cd clean; make)
)
