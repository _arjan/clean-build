#!/bin/sh
set -e
export CUR_CLEAN_HOME=$CLEAN_HOME
export CUR_PATH=$PATH

#### Component build functions ####

build_codegenerator () { # $1:target 
    mkdir -p build
    cp -r src/CodeGenerator build/codegenerator
    (cd build/codegenerator
        make -f Makefile.linux64 cg
    )

    mkdir -p $1/lib/exe
    cp build/codegenerator/cg $1/lib/exe
}

build_runtimesystem_c () { # $1:target
    mkdir -p build
    cp -r src/RuntimeSystem build/runtimesystem
    (cd build/runtimesystem
        ./make_astartup.csh
        ./make_astartupTrace.csh
    )
    mkdir -p $1/lib/StdEnv/Clean\ System\ Files

    touch $1/lib/StdEnv/_startupProfile.dcl
    touch $1/lib/StdEnv/_startupTrace.dcl

    cp build/runtimesystem/linux64/_startup.o $1/lib/StdEnv/Clean\ System\ Files/_startup.o
    cp build/runtimesystem/linux64Trace/_startupTrace.o $1/lib/StdEnv/Clean\ System\ Files/_startupTrace.o
}
build_runtimesystem_clean () { # $1:target
    #Generate the 'clean part' of the runtime system
    mkdir -p $1/lib/StdEnv/Clean\ System\ Files
    cp src/libraries/StdEnv/StdEnv\ 64\ Changed\ Files/_system.abc $1/lib/StdEnv/Clean\ System\ Files/_system.abc
    #Use the new code generator
    target/clean-c-components/lib/exe/cg $1/lib/StdEnv/Clean\ System\ Files/_system
}

build_linker () { # $1:target
    #TODO: set_return_code should not be included from the compiler build directory
    mkdir -p build
    cp -r src/elf_linker build/elf_linker
    (cd build/elf_linker
        clm -I . -I ai64 -IL ArgEnv -I ../../build/compiler/main/Unix \
            -l ../../build/compiler/main/Unix/set_return_code_c.o \
            -s 8m -h 64m -nt -nr linker -o linker
     )

    mkdir -p $1/lib/exe
    cp build/elf_linker/linker $1/lib/exe/linker
}

build_clm () { # $1:target
    mkdir -p build
    cp -r src/tools build/tools
    (cd build/tools/clm
        make -f Makefile.linux64 clm
    )
    mkdir -p $1/bin
    cp build/tools/clm/clm $1/bin/clm
}
build_cpm () { # $1:target

    #Collect dependencies
    mkdir -p build/cpm/deps/platform
    cp -r src/clean-platform/src/libraries/OS-Independent/* build/cpm/deps/platform/
    cp -r src/clean-platform/src/libraries/OS-Posix/* build/cpm/deps/platform/
    cp -r src/clean-platform/src/libraries/OS-Linux/* build/cpm/deps/platform/
    cp -r src/clean-platform/src/libraries/OS-Linux-64/* build/cpm/deps/platform/
    cp -r src/clean-platform/src/libraries/Platform-x86/* build/cpm/deps/platform/

    cp -r src/clean-platform/src/libraries/OS-Independent/Deprecated/StdLib build/cpm/deps/platform-stdlib

    #Get cpm source code
    cp -r src/tools/CleanIDE build/cpm/CleanIDE
    (cd build/cpm/CleanIDE/cpm
        clm -h 256M -nr -nt -I Posix -I ../BatchBuild -I ../Pm -I ../Unix -I ../Util -I ../Interfaces/LinkerInterface\
            -I ../../deps/platform -I ../../deps/platform-stdlib -IL ArgEnv -IL Directory -IL Generics Cpm -o cpm
    )

    mkdir -p $1/bin
    cp build/cpm/CleanIDE/cpm/cpm $1/bin/cpm

    mkdir -p $1/etc
    mkdir -p $1/Temp
    cp txt/IDEEnvs $1/etc/IDEEnvs
}

build_compiler() { # $1:target
    mkdir -p build
    cp -r src/compiler build/compiler
    (cd build/compiler
        clm -ABC -nw -ci -I backend -I frontend -I main/Unix -IL ArgEnv backendconvert
        sed s/80M/256M/ < unix/make.linux64.sh > unix/make.linux64_.sh
        chmod +x unix/make.linux64_.sh
        ./unix/make.linux64_.sh
    )
    mkdir -p $1/lib/exe
    cp build/compiler/cocl $1/lib/exe/cocl
}

build_stdenv() { # $1:target
    mkdir -p $1/lib/StdEnv
    cp src/libraries/StdEnv/_library.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/_startup.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/_system.dcl $1/lib/StdEnv/

    for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
	  StdFunc StdList StdMisc StdOrdList StdOverloaded StdOverloadedList \
	  StdStrictLists StdTuple _SystemArray _SystemEnum _SystemEnumStrict \
	  _SystemStrictLists StdGeneric;
    do cp src/libraries/StdEnv/$a.[di]cl $1/lib/StdEnv ;
    done

    cp src/libraries/StdEnv/StdInt.icl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdInt.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdFile.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdFile.icl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdReal.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdReal.icl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdString.dcl $1/lib/StdEnv/
    cp src/libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdString.icl $1/lib/StdEnv/

    echo first compile of system modules
    for a in StdChar; # compile twice for inlining
    do $CLEAN_HOME/lib/exe/cocl -P $1/lib/StdEnv $a ;
    done

    echo second compile of system modules
    for a in StdMisc StdBool StdInt StdChar StdFile StdReal StdString;
    do $CLEAN_HOME/lib/exe/cocl -P $1/lib/StdEnv $a ;
    done
}

build_argenv() { # $1:target
    mkdir -p build/libraries/
    cp -r src/libraries/ArgEnvUnix build/libraries/ArgEnv
    (cd build/libraries/ArgEnv
        make -e
    )

    mkdir -p "$1/lib/ArgEnv/Clean System Files"
    for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README;
    do  cp build/libraries/ArgEnv/$f $1/lib/ArgEnv/$f
    done
    cp "build/libraries/ArgEnv/Clean System Files/ArgEnvC.o" "$1/lib/ArgEnv/Clean System Files/"
}

build_dynamics() { #$1:target
    #Copy minimal Dynamics libraries
    mkdir -p "$1/lib/Dynamics/Clean System Files"
    cp src/libraries/StdDynamicEnv/extension/StdCleanTypes.dcl $1/lib/Dynamics/
    cp src/libraries/StdDynamicEnv/extension/StdCleanTypes.icl $1/lib/Dynamics/
    cp src/libraries/StdDynamicEnv/extension/StdDynamic.dcl $1/lib/Dynamics/
    cp src/libraries/StdDynamicEnv/extension/StdDynamicNoLinker.icl $1/lib/Dynamics/StdDynamic.icl
    cp src/libraries/StdDynamicEnv/implementation/_SystemDynamic.dcl $1/lib/Dynamics/
    cp src/libraries/StdDynamicEnv/implementation/_SystemDynamic.icl $1/lib/Dynamics/
}
build_generics() { #$1:target
    #Copy generics library
    mkdir -p $1/lib/Generics
    cp src/libraries/GenLib/*.[id]cl $1/lib/Generics/
}
build_directory() {
    mkdir -p build/Directory
    cp -r src/libraries/Directory/* build/Directory/
    cp src/tools/htoclean/Clean.h "build/Directory/Clean System Files Unix/Clean.h"

    (cd "build/Directory/Clean System Files Unix"
         gcc -c -O cDirectory.c
    )
    mkdir -p "$1/lib/Directory/Clean System Files"
    cp build/Directory/*.[id]cl $1/lib/Directory/
    cp build/Directory/Clean\ System\ Files\ Unix/* $1/lib/Directory/Clean\ System\ Files/
}

build_clean_c_components () { # $1:target
    mkdir -p $1
    build_codegenerator $1
    build_runtimesystem_c $1
    build_clm $1
}

build_clean() { # $1:target
    mkdir -p $1

    #Copy all C components (they are built only once)
    cp -r target/clean-c-components/* $1/

    #Copy docs
    cp src/tools/CleanIDE/CleanLicenseConditions.txt $1/CleanLicenseConditions.txt
    cp txt/README $1/README.md

    mkdir -p $1/doc
    cp src/language_report/CleanLangRep.2.2.pdf $1/doc/
    mkdir -p $1/doc/CleanLangRep/
    cp src/language_report/*.htm $1/doc/CleanLangRep/
    cp src/language_report/*.css $1/doc/CleanLangRep/
    cp src/language_report/*.png $1/doc/CleanLangRep/
    mkdir -p $1/doc/CleanLangRep/CleanRep.2.2_files/
    cp src/language_report/CleanRep.2.2_files/* $1/doc/CleanLangRep/CleanRep.2.2_files/

    # Build all Clean components
    # Standard libraries
    build_stdenv $1
    # Rts and compiler
    build_runtimesystem_clean $1
    build_compiler $1
    build_linker $1
}

#### Main build process ####

# Build components written in C (code generator, runtime system and clm)
build_clean_c_components target/clean-c-components

# Temporary workaround until a boot_compiler can be used that has a recent enough clm version that supports the CLEAN_HOME variable
build_clm dependencies/clean

# First pass: build a clean system using the boot compiler
export PATH=`pwd`/dependencies/clean/bin:$CUR_PATH
export CLEAN_HOME=`pwd`/dependencies/clean

build_clean target/clean-intermediate
# Standard libraries as dependencies for the second pass
build_dynamics target/clean-intermediate
build_generics target/clean-intermediate
build_directory target/clean-intermediate
build_argenv target/clean-intermediate

# Second pass: Build a clean system using the clean system from the previous pass
export PATH=`pwd`/target/clean-intermediate/bin:$CUR_PATH
export CLEAN_HOME=`pwd`/target/clean-intermediate

build_clean target/clean-base
# Add cpm 
build_cpm target/clean-base
