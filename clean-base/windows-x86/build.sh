#!/bin/sh
set -e
export CUR_CLEAN_HOME=$CLEAN_HOME
export CUR_PATH=$PATH

#Visual studio compiler (only included for building temporary ObjectIO)
VSROOT="C:\Program Files\Microsoft Visual Studio 14.0"
VSDEVENV=`cygpath --unix "$VSROOT\Common7\IDE\devenv.exe"`
VSVARS="$VSROOT\VC\bin\vcvars32.bat"
VSCC="$VSROOT\VC\bin\cl.exe"

#### Component build functions ####

build_codegenerator () { # $1:target 
    mkdir -p build
    cp -r src/CodeGenerator build/codegenerator
    cd build/codegenerator
        make -f Makefile.windows_mingw 
    cd ../..

    mkdir -p "$1/Tools/Clean System"
    cp build/codegenerator/cg.exe "$1/Tools/Clean System/CodeGenerator.exe"
}
build_compiler_backend () { # $1: target
    mkdir -p build
    cp -r src/compiler/backendC build/backend
    cd build/backend/CleanCompilerSources
        make -f Makefile.windows_mingw
    cd ../../..

    cp build/backend/backend.dll "$1/Tools/Clean System/backend.dll"
}

build_runtimesystem_c () { # $1:target
    mkdir -p build
    cp -r src/RuntimeSystem build/runtimesystem

    cd build/runtimesystem
    	chmod +x build_windows_object_files.sh
    	./build_windows_object_files.sh
    cd ../..

    mkdir -p $1/Libraries/StdEnv/Clean\ System\ Files

    touch $1/Libraries/StdEnv/_startupProfile.dcl
    touch $1/Libraries/StdEnv/_startupTrace.dcl

    for objectfile in _startup1.o _startup1Profile.o _startup1Trace.o _startup2.o ; do
        cp build/runtimesystem/$objectfile "$1/Libraries/StdEnv/Clean System Files/"
    done
}
build_runtimesystem_clean () { # $1:target
    #Generate the 'clean part' of the runtime system
    mkdir -p $1/Libraries/StdEnv/Clean\ System\ Files

    cp src/libraries/StdEnv/Clean\ System\ Files/_system.abc $1/Libraries/StdEnv/Clean\ System\ Files/_system.abc

    #Copy binary objectfiles
    cp src/libraries/StdEnv/Object\ Files\ Windows\ 32/_startup0.o $1/Libraries/StdEnv/Clean\ System\ Files/
    cp src/libraries/StdEnv/Object\ Files\ Windows\ 32/*_library $1/Libraries/StdEnv/Clean\ System\ Files/

    #Use the new code generator
    target/clean-c-components/Tools/Clean\ System/CodeGenerator.exe $1/Libraries/StdEnv/Clean\ System\ Files/_system
}

build_linker () { # $1:target $2: clean system
    mkdir -p build/linker
    cp -r src/dynamics/* build/linker/

    rm -rf build/clean
    mkdir -p build/clean
    cp -r $2/* build/clean/

    #Patched project file for the linker
    cp txt/StaticLinker.prj build/linker/StaticLinker.prj

    cd build/linker
	LINKER_PRJ=`cygpath --windows --absolute "StaticLinker.prj"`
	#Try cpm first, otherwise try the clean ide
	if test -f ../clean/cpm.exe; then
           ../clean/cpm.exe StaticLinker.prj
	else
           ../clean/CleanIDE.exe --batch-build $LINKER_PRJ
	fi
    cd ../..

    mkdir -p "$1/Tools/Clean System"
    cp build/linker/StaticLinker.exe "$1/Tools/Clean System/StaticLinker.exe"
}

build_clm () { # $1:target
    mkdir -p build
    cp -r src/tools build/tools
    cd build/tools/clm

	#increase clm path size (or else it will crash when building cpm)
	sed "s/PATH_NAME_STRING_SIZE 256/PATH_NAME_STRING_SIZE 1024/" < clm.c > clm_.c
	mv clm_.c clm.c

        make -f Makefile.windows-x86 clm
    cd ../../..

    mkdir -p $1/Tools
    cp build/tools/clm/clm.exe $1/Tools/clm.exe
}
build_cpm_intermediate () {
    echo "CPM INTERMEDIATE"
    #Copy the clean system for building
    mkdir -p build/cpm/clean-build
    mkdir -p build/cpm/clean-target

    cp -r dependencies/clean/* build/cpm/clean-build/
    cp -r target/clean-intermediate/* build/cpm/clean-target/

    #Get cpm source code
    mkdir -p build/cpm/CleanIDE
    cp -r src/tools/CleanIDE/* build/cpm/CleanIDE/

    #Patch project file
    cp txt/CpmWin.prj build/cpm/CleanIDE/CpmWin.prj

    #Create BatchBuild to bootstrap
	
    # Use 16M heap for batchbuild instead of 4M
    sed s/HeapSize:\\t4194304/HeapSize:\\t16777216/ < build/cpm/CleanIDE/BatchBuild.prj > build/cpm/CleanIDE/BatchBuild_.prj
    BATCHBUILD_PRJ=`cygpath --windows --absolute "build/cpm/CleanIDE/BatchBuild_.prj"`

    build/cpm/clean-build/CleanIDE.exe --batch-build $BATCHBUILD_PRJ
    cp build/cpm/CleanIDE/BatchBuild.exe build/cpm/clean-target/

    # Wait to make sure all batchbuild fully terminated
    sleep 1

    #Use batchbuild to create cpm
    CPM_PRJ=`cygpath --windows --absolute "build/cpm/CleanIDE/CpmWin.prj"`
    build/cpm/clean-target/BatchBuild.exe $CPM_PRJ

    #mkdir -p $1/Tools/Temp
    cp build/cpm/CleanIDE/cpm/cpm.exe target/clean-intermediate/cpm.exe

    mkdir -p target/clean-intermediate/Config
    cp txt/IDEEnvs target/clean-intermediate/Config/IDEEnvs
}

build_cpm_base() {
    echo "CPM BASE"

    #Copy the clean system for building
    mkdir -p build/cpm/clean-build

    cp -r target/clean-intermediate/* build/cpm/clean-build/

    #Get cpm source code
    mkdir -p build/cpm/CleanIDE
    cp -r src/tools/CleanIDE/* build/cpm/CleanIDE/

    #Patch project file
    cp txt/CpmWin.prj build/cpm/CleanIDE/CpmWin.prj

    #Build
    cd build/cpm/CleanIDE
       ../clean-build/cpm.exe CpmWin.prj
    cd ../../..

    #mkdir -p $1/Tools/Temp
    cp build/cpm/CleanIDE/cpm/cpm.exe target/clean-base/cpm.exe

    mkdir -p target/clean-base/Config
    cp txt/IDEEnvs target/clean-base/Config/IDEEnvs
}

build_compiler() { # $1:target $2:clean system
    mkdir -p build/compiler
    cp -r src/compiler/* build/compiler/

    rm -rf build/clean
    mkdir -p build/clean
    cp -r $2/* build/clean/

    cp build/compiler/backendC/CleanCompilerSources/backend.h build/compiler/backend/backend.h
    
    #TODO: Use an intermediately built htoclean

    BACKEND_H=`cygpath --windows --absolute "build/compiler/backend/backend.h"`
    dependencies/clean/Tools/htoclean/htoclean.exe $BACKEND_H

    cd build/compiler
        sed s/HeapSize:\\t20971520/HeapSize:\\t134217728/ < coclmaindll/cocl.prj > cocl.prj
	mkdir -p "Clean System Files"

	COCL_PRJ=`cygpath --windows --absolute "cocl.prj"`
	echo $COCL_PRJ

	#Try cpm first, otherwise try the clean ide
	if test -f ../clean/cpm.exe; then
           ../clean/cpm.exe cocl.prj
	else
           ../clean/CleanIDE.exe --batch-build $COCL_PRJ
	fi
    cd ../..

    mkdir -p "$1/Tools/Clean System"
    cp build/compiler/cocl.exe "$1/Tools/Clean System/CleanCompiler.exe"
}

build_stdenv() { # $1:target $2: clean system
    mkdir -p $1/Libraries/StdEnv

    cp src/libraries/StdEnv/_library.dcl $1/Libraries/StdEnv/
    cp src/libraries/StdEnv/_startup.dcl $1/Libraries/StdEnv/
    cp src/libraries/StdEnv/_system.dcl $1/Libraries/StdEnv/

    for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
	  StdInt StdFile StdReal StdString \
	  StdFunc StdList StdMisc StdOrdList StdOverloaded StdOverloadedList \
	  StdStrictLists StdTuple _SystemArray _SystemEnum _SystemEnumStrict \
	  _SystemStrictLists StdGeneric;
    do cp src/libraries/StdEnv/$a.[di]cl $1/Libraries/StdEnv/ ;
    done

    STDENV_SYSTEM_MODULES="StdBool StdChar StdFile StdInt StdMisc StdReal StdString"
    for stdenv_build_m in $STDENV_SYSTEM_MODULES; do
        "$2/Tools/Clean System/CleanCompiler.exe" -P "$1/Libraries/StdEnv/$stdenv_build_m" -dynamic;
    done
    for stdenv_build_m in $STDENV_SYSTEM_MODULES; do
        "$2/Tools/Clean System/CleanCompiler.exe" -P "$1/Libraries/StdEnv/$stdenv_build_m"; 
    done
}

build_argenv() { # $1:target
    mkdir -p build/libraries/
    cp -r src/libraries/ArgEnvWindows build/libraries/ArgEnv
    #TODO: Should compile the c part of this library and not use object file from svn
    #(cd build/libraries/ArgEnv
    #    gcc -c ArgEnvC.c -o ArgEnvC.o
    #)

    mkdir -p "$1/Libraries/ArgEnv/Clean System Files"
    for f in ArgEnvC.c ArgEnv.dcl printenv.icl ArgEnv.icl README.txt;
    do  cp build/libraries/ArgEnv/$f $1/Libraries/ArgEnv/$f
    done
    cp build/libraries/ArgEnv/Clean\ System\ Files/* "$1/Libraries/ArgEnv/Clean System Files/"
    #cp "build/libraries/ArgEnv/ArgEnvC.o" "$1/Libraries/ArgEnv/Clean System Files/"
}

build_dynamics() { #$1:target
    #Copy minimal Dynamics libraries
    mkdir -p "$1/Libraries/Dynamics/Clean System Files"
    cp src/libraries/StdDynamicEnv/extension/StdCleanTypes.dcl $1/Libraries/Dynamics/
    cp src/libraries/StdDynamicEnv/extension/StdCleanTypes.icl $1/Libraries/Dynamics/
    cp src/libraries/StdDynamicEnv/extension/StdDynamic.dcl $1/Libraries/Dynamics/
    cp src/libraries/StdDynamicEnv/extension/StdDynamicNoLinker.icl $1/Libraries/Dynamics/StdDynamic.icl
    cp src/libraries/StdDynamicEnv/implementation/_SystemDynamic.dcl $1/Libraries/Dynamics/
    cp src/libraries/StdDynamicEnv/implementation/_SystemDynamic.icl $1/Libraries/Dynamics/
}
build_generics() { #$1:target
    #Copy generics library
    mkdir -p $1/Libraries/Generics
    cp src/libraries/GenLib/*.[id]cl $1/Libraries/Generics/
}
build_directory() { #$1:target
    mkdir -p build/Directory
    cp -r src/libraries/Directory/* build/Directory/
    cp src/tools/htoclean/Clean.h "build/Directory/Clean System Files Windows/Clean.h"

    cd "build/Directory/Clean System Files Windows"
         gcc -c -O cDirectory.c
    cd ../../..

    mkdir -p "$1/Libraries/Directory/Clean System Files"
    cp build/Directory/*.[id]cl $1/Libraries/Directory/
    cp build/Directory/Clean\ System\ Files\ Windows/* $1/Libraries/Directory/Clean\ System\ Files/
}
build_stdlib() { #$1:target
    #Copy generics library
    mkdir -p $1/Libraries/StdLib
    cp src/libraries/StdLib/*.[id]cl $1/Libraries/StdLib/
}
build_objectio() { #$1:target
    mkdir -p build/ioobjects
    cp -r "src/libraries/ObjectIO/ObjectIO/OS Windows/Windows_C_12" "build/objectio"

    #Compile c files
    for c in build/objectio/*.c; do
        vscc "$c"
    done

    #Copy .o files to target
    mkdir -p "$1/Libraries/ObjectIO/OS Windows/Clean System Files"
    cp build/objectio/*.o "$1/Libraries/ObjectIO/OS Windows/Clean System Files/"
    # Copy clean files to target
    cp src/libraries/ObjectIO/ObjectIO/*.[id]cl $1/Libraries/ObjectIO/
    cp src/libraries/ObjectIO/ObjectIO/OS\ Windows/*.[id]cl $1/Libraries/ObjectIO/OS\ Windows/
    cp src/libraries/ObjectIO/ObjectIO/OS\ Windows/Clean\ System\ Files/*_library $1/Libraries/ObjectIO/OS\ Windows/Clean\ System\ Files/
}

vscc() { # $1: C .c file path

	COMMAND=`cygpath --unix $COMSPEC`
	vscc_file=`cygpath --absolute --windows "$1"`
	vscc_dir=`dirname "$1"`
	vscc_object_file=`basename $vscc_file .c`.o
	(cd "$vscc_dir"
	 cat <<EOBATCH | "$COMMAND"
@"$VSVARS"
@cl /nologo /c /GS- "$vscc_file" /Fo"$vscc_object_file"
EOBATCH
	)
}
build_platform () { # $1:target

    mkdir -p $1/Libraries/Platform
    cp -r src/clean-platform/src/libraries/OS-Independent/* $1/Libraries/Platform/
    cp -r src/clean-platform/src/libraries/OS-Windows/* $1/Libraries/Platform/
    cp -r src/clean-platform/src/libraries/OS-Windows-32/* $1/Libraries/Platform/
    cp -r src/clean-platform/src/libraries/Platform-x86/* $1/Libraries/Platform/
}

build_clean_c_components () { # $1:target
    mkdir -p $1
    build_codegenerator $1
    build_compiler_backend $1
    build_runtimesystem_c $1
    build_clm $1
}

build_clean() { # $1:target $2: clean system
    mkdir -p $1

    #Copy all C components (they are built only once)
    cp -r target/clean-c-components/* $1/

    #Create a Temp directory for compiler output
    mkdir -p $1/Temp

    #Copy docs
    cp src/tools/CleanIDE/CleanLicenseConditions.txt $1/CleanLicenseConditions.txt
    #cp txt/README $1/README.md

    mkdir -p $1/Help
    cp src/language_report/CleanLangRep.2.2.pdf $1/Help/
    mkdir -p $1/Help/CleanLangRep/
    cp src/language_report/*.htm $1/Help/CleanLangRep/
    cp src/language_report/*.css $1/Help/CleanLangRep/
    cp src/language_report/*.png $1/Help/CleanLangRep/
    mkdir -p $1/Help/CleanLangRep/CleanRep.2.2_files/
    cp src/language_report/CleanRep.2.2_files/* $1/Help/CleanLangRep/CleanRep.2.2_files/

    # Build all Clean components
    # Standard libraries
    build_stdenv $1 $2
    # Rts and compiler
    build_runtimesystem_clean $1 $2
    build_compiler $1 $2
    build_linker $1 $2
}

#### Main build process ####

# Build components written in C (code generator, runtime system and clm)
build_clean_c_components target/clean-c-components

# Temporary workaround until a bootstrap clean system can be used that has clm included
build_clm dependencies/clean

# First pass: build a clean system using the bootstrap clean system
mkdir -p build
cp -r dependencies/clean build/clean-build
export PATH=`pwd`/build/clean-build/Tools:$CUR_PATH
export CLEAN_HOME=`cygpath --windows --absolute build/clean-build`

build_clean target/clean-intermediate build/clean-build
# Standard libraries as dependencies for the second pass
build_dynamics target/clean-intermediate build/clean-build
build_generics target/clean-intermediate build/clean-build
build_directory target/clean-intermediate build/clean-build
build_argenv target/clean-intermediate build/clean-build
build_stdlib target/clean-intermediate build/clean-build
build_objectio target/clean-intermediate build/clean-build
build_platform target/clean-intermediate build/clean-build

# Build cpm using the freshly built intermediate clean system
# and a little help from the bootstrap clean system
build_cpm_intermediate

# Second pass: Build a clean system using the clean system from the previous pass
export PATH=`pwd`/target/clean-intermediate/Tools:$CUR_PATH
export CLEAN_HOME=`cygpath --windows --absolute target/clean-intermediate`

rm -rf build

build_clean target/clean-base target/clean-intermediate
build_cpm_base
