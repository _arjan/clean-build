#!/bin/sh
SVN=svn
SVN_BASEURL="https://svn.cs.ru.nl/repos"

mkdir -p src
$SVN export "$SVN_BASEURL/clean-libraries/trunk/Libraries/Directory" "src/Directory"
$SVN export "$SVN_BASEURL/clean-tools/trunk/htoclean" "src/htoclean"
