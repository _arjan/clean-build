#!/bin/sh
set -e
cp -r "step2/dist" "test"
(cd "test"
 # Compile unit tests
 cp Libraries/iTasks-SDK/Tests/RunTests.prj.default Libraries/iTasks-SDK/Tests/RunTests.prj
 ./CleanIDE.exe --batch-build `cygpath --windows --absolute Libraries/iTasks-SDK/Tests/RunTests.prj`
 # Execute unit tests
 ./Libraries/iTasks-SDK/Tests/RunTests.exe --unit
 # Compile examples
 cp Libraries/iTasks-SDK/Examples/BasicAPIExamples.prj.default Libraries/iTasks-SDK/Examples/BasicAPIExamples.prj
 ./CleanIDE.exe --batch-build `cygpath --windows --absolute Libraries/iTasks-SDK/Examples/BasicAPIExamples.prj`
)
