set -e
# ./checkout.sh libraries/ArgEnvUnix
if test ! -d libraries/ArgEnvUnix ; then
  ./svn_checkout.sh clean-libraries/trunk/Libraries/ArgEnvUnix libraries/ArgEnvUnix
fi
# ./checkout.sh compiler
./svn_checkout.sh clean-compiler/branches/itask compiler

cd compiler
clm -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert
cd unix
sed "s/-h 80M -s 4m/-h 256m -s 8m/" <make.macosx64.sh >make.macosx64.sh_
rm -f make.macosx64.sh
mv make.macosx64.sh_ make.macosx64.sh
chmod +x make.macosx64.sh
cd ..
unix/make.macosx64.sh
cd ..

