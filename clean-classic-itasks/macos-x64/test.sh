set -e
cp -r "clean" "test"
(cd "test"
 export CLEAN_HOME=`pwd`
 make
 # Compile unit tests
 cp lib/iTasks-SDK/Tests/RunTests.prj.default lib/iTasks-SDK/Tests/RunTests.prj
 bin/cpm lib/iTasks-SDK/Tests/RunTests.prj
 # Execute unit tests
 lib/iTasks-SDK/Tests/RunTests.exe --unit
 # Compile examples
 cp lib/iTasks-SDK/Examples/BasicAPIExamples.prj.default lib/iTasks-SDK/Examples/BasicAPIExamples.prj
 bin/cpm lib/iTasks-SDK/Examples/BasicAPIExamples.prj
)
