set -e
if test ! -d platform ; then
  git clone --depth 1 https://gitlab.science.ru.nl/clean-and-itasks/clean-platform.git
  mv clean-platform/src/libraries platform
  rm -rf clean-platform
fi

