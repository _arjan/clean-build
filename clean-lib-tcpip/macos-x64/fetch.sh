#/bin/sh
SVN=svn
SVN_BASEURL="https://svn.cs.ru.nl/repos"
GIT=git
GIT_BASEURL="https://gitlab.science.ru.nl/clean-and-itasks"

mkdir -p src
$SVN export "$SVN_BASEURL/clean-libraries/trunk/Libraries/TCPIP" "src/TCPIP"
$SVN export "$SVN_BASEURL/clean-tools/trunk/htoclean" "src/htoclean"
