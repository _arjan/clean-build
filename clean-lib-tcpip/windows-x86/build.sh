#!/bin/sh

TARGET_DIR=target/clean-lib-tcpip/Libraries/TCPIP

#Visual studio compiler (only included for building temporary ObjectIO)
VSROOT="C:\Program Files\Microsoft Visual Studio 14.0"
VSDEVENV=`cygpath --unix "$VSROOT\Common7\IDE\devenv.exe"`
VSVARS="$VSROOT\VC\bin\vcvars32.bat"
VSCC="$VSROOT\VC\bin\cl.exe"

vscc() { # $1: C .c file path

	COMMAND=`cygpath --unix $COMSPEC`
	vscc_file=`cygpath --absolute --windows "$1"`
	vscc_dir=`dirname "$1"`
	vscc_object_file=`basename $vscc_file .c`.o
	(cd "$vscc_dir"
	 cat <<EOBATCH | "$COMMAND"
@"$VSVARS"
@cl /nologo /c /GS- "$vscc_file" /Fo"$vscc_object_file"
EOBATCH
	)
}

# Copy Clean modules
mkdir -p $TARGET_DIR
cp src/TCPIP/TCPIP.dcl $TARGET_DIR
cp src/TCPIP/TCPIP.icl $TARGET_DIR
cp src/TCPIP/TCPDef.icl $TARGET_DIR
cp src/TCPIP/TCPDef.dcl $TARGET_DIR
cp src/TCPIP/TCPEvent.dcl $TARGET_DIR
cp src/TCPIP/TCPEvent.icl $TARGET_DIR
cp src/TCPIP/TCPChannelClass.dcl $TARGET_DIR
cp src/TCPIP/TCPChannelClass.icl $TARGET_DIR
cp src/TCPIP/TCPChannels.dcl $TARGET_DIR
cp src/TCPIP/TCPChannels.icl $TARGET_DIR
cp src/TCPIP/TCPStringChannels.dcl $TARGET_DIR
cp src/TCPIP/TCPStringChannels.icl $TARGET_DIR
cp src/TCPIP/TCPStringChannelsInternal.dcl $TARGET_DIR
cp src/TCPIP/TCPStringChannelsInternal.icl $TARGET_DIR
cp src/TCPIP/tcp_bytestreams.dcl $TARGET_DIR
cp src/TCPIP/tcp_bytestreams.icl $TARGET_DIR
cp src/TCPIP/tcp.dcl $TARGET_DIR
cp src/TCPIP/tcp.icl $TARGET_DIR
cp src/TCPIP/ostcp.dcl $TARGET_DIR
cp src/TCPIP/ostcp.icl $TARGET_DIR

mkdir -p $TARGET_DIR/Clean\ System\ Files/
cp src/TCPIP/Clean\ System\ Files/* $TARGET_DIR/Clean\ System\ Files/

# Build and copy C interface
mkdir -p build/TCPIP/csf

cp src/TCPIP/Windows_C/*.c build/TCPIP/csf/
cp src/TCPIP/Windows_C/*.h build/TCPIP/csf/
#Additional headers
cp src/ObjectIO/ObjectIO/OS\ Windows/Windows_C_12/*.h build/TCPIP/csf/
cp src/htoclean/Clean.h build/TCPIP/csf/

(cd build/TCPIP/csf
     vscc cTCP_121.c
     vscc cCrossCallTCP_121.c
)

mkdir -p $TARGET_DIR/Clean\ System\ Files/
cp build/TCPIP/csf/cTCP_121.o $TARGET_DIR/Clean\ System\ Files/
cp build/TCPIP/csf/cCrossCallTCP_121.o $TARGET_DIR/Clean\ System\ Files/

