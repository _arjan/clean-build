#/bin/sh
SVN=svn
SVN_BASEURL="https://svn.cs.ru.nl/repos"

REV=""
if [ -n "${CLEANDATE+set}" ]; then
	REV="-r {$CLEANDATE}"
fi

mkdir -p src
rm -rf src/TCPIP src/htoclean
$SVN export $REV "$SVN_BASEURL/clean-libraries/trunk/Libraries/TCPIP" "src/TCPIP"
$SVN export $REV "$SVN_BASEURL/clean-tools/trunk/htoclean" "src/htoclean"
