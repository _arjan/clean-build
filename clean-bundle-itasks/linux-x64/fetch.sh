#!/bin/sh
# Get the latest clean-base system plus:
LIBRARIES="generics dynamics platform sapl graphcopy tcpip gast itasks"

mkdir -p src
# Download and unpack clean base
curl -L -o src/clean-base.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/`date +clean-base-linux-x64-%Y%m%d.tgz`
(cd src; tar -xzf clean-base.tgz)
# Download and unpack additional libraries
for LIB in $LIBRARIES; do
	echo $LIB
	../../clean-lib-$LIB/linux-x64/fetch.sh
done
