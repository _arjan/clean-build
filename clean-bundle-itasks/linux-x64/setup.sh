#!/bin/sh
LIBRARIES="generics dynamics platform sapl graphcopy tcpip gast itasks"

for LIB in $LIBRARIES; do
	../../clean-lib-$LIB/linux-x64/setup.sh
done
