#!/bin/sh
LIBRARIES="generics dynamics platform sapl graphcopy tcpip gast itasks"

mkdir -p target/clean-bundle-itasks
# Add base system
cp -r src/clean-base/* target/clean-bundle-itasks/
# Add additional libraries
for LIB in $LIBRARIES; do
	../../clean-lib-$LIB/linux-x64/build.sh
	rsync -r target/clean-lib-$LIB/* target/clean-bundle-itasks
	rm -rf target/clean-lib-$LIB
done
# Install environments
for ENV in target/clean-bundle-itasks/etc/*.env; do
	tail -n +2 $ENV >> target/clean-bundle-itasks/etc/IDEEnvs	
done
