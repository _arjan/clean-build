#!/bin/sh
# Get the latest clean-base system plus:
LIBRARIES="generics dynamics platform sapl graphcopy tcpip gast itasks"

mkdir -p src
# Download and unpack clean base
curl -L -o src/clean-base.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-base-windows-x86-%Y%m%d.zip`
(cd src; unzip clean-base.zip)
# Download and unpack additional libraries
for LIB in $LIBRARIES; do
	curl -L -o src/clean-lib-$LIB.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-lib-$LIB-windows-x86-%Y%m%d.zip`
	(cd src; unzip clean-lib-$LIB.zip)
done
# Add Clean IDE
curl -L -o src/clean-ide.zip ftp://ftp.cs.ru.nl/pub/Clean/builds/windows-x86/`date +clean-ide-windows-x86-%Y%m%d.zip`
(cd src; unzip clean-ide.zip)

