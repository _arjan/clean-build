#!/bin/sh
LIBRARIES="generics dynamics platform sapl graphcopy tcpip gast itasks"

mkdir -p target/clean-bundle-itasks
# Add base system
cp -r src/clean-base/* target/clean-bundle-itasks/
# Add additional libraries
for LIB in $LIBRARIES; do
	cp -r src/clean-lib-$LIB/* target/clean-bundle-itasks/
done
# Install environments
for ENV in target/clean-bundle-itasks/Config/*.env; do
	tail -n +2 $ENV >> target/clean-bundle-itasks/Config/IDEEnvs	
done
# Add Clean IDE
cp -r src/clean-ide/* target/clean-bundle-itasks/
