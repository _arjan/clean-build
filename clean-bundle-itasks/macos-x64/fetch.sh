#!/bin/sh
# Get the latest clean-base system plus:
LIBRARIES="generics dynamics platform sapl graphcopy tcpip gast itasks"

mkdir -p src
# Download and unpack clean base
curl -L -o src/clean-base.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-base-macos-x64-%Y%m%d.tgz`
(cd src; tar -xzf clean-base.tgz)
# Download and unpack additional libraries
for LIB in $LIBRARIES; do
	curl -L -o src/clean-lib-$LIB.tgz ftp://ftp.cs.ru.nl/pub/Clean/builds/macos-x64/`date +clean-lib-$LIB-macos-x64-%Y%m%d.tgz`
	(cd src; tar -xzf clean-lib-$LIB.tgz)
done
