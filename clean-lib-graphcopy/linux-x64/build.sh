#!/bin/sh
mkdir -p target/clean-lib-graphcopy

# Add clean libraries
mkdir -p target/clean-lib-graphcopy/lib/GraphCopy
cp -r src/clean-graph-copy/common/* target/clean-lib-graphcopy/lib/GraphCopy/
cp -r src/clean-graph-copy/linux/*.[id]cl target/clean-lib-graphcopy/lib/GraphCopy/

# Build c library
mkdir -p build/clean-graph-copy
cp -r src/clean-graph-copy/common build/clean-graph-copy/
cp -r src/clean-graph-copy/linux build/clean-graph-copy/
cp -r src/clean-graph-copy/linux64 build/clean-graph-copy/
cp -r src/clean-graph-copy/tools build/clean-graph-copy/
(cd build/clean-graph-copy/linux64
    make -e
)
cp -r build/clean-graph-copy/linux64/Clean\ System\ Files target/clean-lib-graphcopy/lib/GraphCopy/
