#!/bin/sh
GIT=git
GIT_BASEURL="https://gitlab.science.ru.nl/clean-and-itasks"

mkdir -p src
rm -rf src/clean-graph-copy
$GIT clone $GIT_BASEURL/clean-graph-copy src/clean-graph-copy
if [ -n "${CLEANDATE+set}" ]; then
	cd "src/clean-graph-copy"
	$GIT checkout `$GIT rev-list -n 1 --before="$CLEANDATE" master`
fi
