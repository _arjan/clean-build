#!/bin/sh
GIT=git
GIT_BASEURL="https://gitlab.science.ru.nl/clean-and-itasks"

mkdir -p src
$GIT clone --depth 1 $GIT_BASEURL/clean-graph-copy src/clean-graph-copy
