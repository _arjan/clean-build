#!/bin/sh
mkdir -p target/clean-lib-graphcopy

# Add clean libraries
mkdir -p target/clean-lib-graphcopy/Libraries/GraphCopy
cp -r src/clean-graph-copy/common/* target/clean-lib-graphcopy/Libraries/GraphCopy/
cp -r src/clean-graph-copy/windows-x86/*.[id]cl target/clean-lib-graphcopy/Libraries/GraphCopy/

# Build c library
mkdir -p build/clean-graph-copy
cp -r src/clean-graph-copy/common build/clean-graph-copy/
cp -r src/clean-graph-copy/win32 build/clean-graph-copy/
cp -r src/clean-graph-copy/tools build/clean-graph-copy/
(cd build/clean-graph-copy/win32
    make -e
)
cp -r build/clean-graph-copy/win32/Clean\ System\ Files target/clean-lib-graphcopy/Libraries/GraphCopy/
